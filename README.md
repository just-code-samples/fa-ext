# fastapi-core


### Commands

- Build Docker container:  
    `docker build -t fa .`

- Run Docker container:  
    `docker run --name fa-cont -p 5000:5000 fa`


### Endpoints

- By default: `0.0.0.0:5000`
- Docs:
    - Swagger: `/docs`
    - Redoc: `/redoc`
