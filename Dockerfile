FROM python:3.8.5

ENV TZ=Asia/Yekaterinburg
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN pip install --upgrade pip

WORKDIR /fastapi-core
COPY ./requirements.txt /fastapi-core/
RUN pip install -r requirements.txt


COPY ./main.py /fastapi-core

ENTRYPOINT ["python", "main.py"]

EXPOSE 5000/tcp
